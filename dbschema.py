from google.appengine.ext import ndb


class timelog(ndb.Model):
    currentDate = ndb.DateProperty(auto_now=True)
    currentTime = ndb.TimeProperty()
    username = ndb.StringProperty()
    action = ndb.StringProperty()
    ipaddr = ndb.StringProperty()
    
    
class nets(ndb.Model):
    location = ndb.StringProperty()
    ipAddress = ndb.StringProperty()