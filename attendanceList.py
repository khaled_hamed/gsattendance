from google.appengine.ext import ndb
import jinja2
import os
import webapp2


jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])

class listAttendance(webapp2.RequestHandler):
    def get(self):
        result = ndb.gql("select * from timelog")
        result_values = {'result': result}
        template = jinja_environment.get_template('list.html')
        self.response.out.write(template.render(result_values))
        
