from dbschema import nets, timelog
from google.appengine.api import users
from google.appengine.ext import ndb
import datetime
import jinja2
import os
import webapp2
from attendanceList import listAttendance



jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

class InsertIP(webapp2.RequestHandler):
    def get(self):
        template = jinja_environment.get_template('newip.html')
        self.response.out.write(template.render())
        
class NewIP(webapp2.RequestHandler):
    def post(self):
        newnet = nets()
        newnet.location = self.request.get("location")
        newnet.ipAddress = self.request.get("ipaddress")
        newnet.put()
        self.redirect("/insertip")

class ThankYou(webapp2.RequestHandler):
    def get(self):
        template = jinja_environment.get_template('thankyou.html')
        self.response.out.write(template.render())
        

class Response(webapp2.RequestHandler):
    def post(self):
        entity = timelog()
        
        entity.currentTime = datetime.datetime.time(datetime.datetime.now() + 
                                                    datetime.timedelta(0, 0, 0, 0, 0, 2))
        entity.username = users.get_current_user().email()
        entity.action = self.request.get("check")
        entity.ipaddr = self.request.remote_addr
        entity.put()
        self.redirect('/thankyou')
        
class MainPage(webapp2.RequestHandler):
    
    
    def get(self):
        query = ndb.gql("select ipAddress from nets")
        for li in query:
            if self.request.remote_addr in li.ipAddress:
                template = jinja_environment.get_template('index.html')
                self.response.out.write(template.render())
                break
            else:
                self.response.write(
                                    "<center><h1><p>Error Wrong IP %s" 
                                    "</p></h1></center"
                                    % self.request.remote_addr)


app = webapp2.WSGIApplication([('/', MainPage),
                              ('/response', Response),
                              ('/thankyou', ThankYou),
                              ('/newip', NewIP),
                              ('/insertip', InsertIP),
                            ('/list', listAttendance)
                              ],
                              debug=True)


def main():
    app.run()
    
if __name__ == "__main__":
    main()
